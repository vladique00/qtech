#include <ev.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>
#include <err.h>

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "server.h"
#include "clients.h"

static void accept_connect(struct ev_loop *loop, ev_io *w, int revents);
static void on_read(struct ev_loop *loop, ev_io *w, int revents);
static int send_data(client *c, buffer_t *pkt);
static int read_data(client *c, buffer_t *pkt);
static int listen_on_socket(struct sockaddr_in *listen_addr);

/**
 * @brief Bind and listen for new connections on created socket.
 *
 * @param[in] listen_addr Pointer to sockaddr_in.
 * @return New listen FD or -1 in case of error.
 */
static int listen_on_socket(struct sockaddr_in *listen_addr)
{
        int listen_fd;
        int on = 1;

        listen_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
        if (listen_fd < 0)
                return -1;

        setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
        setsockopt(listen_fd, IPPROTO_TCP, TCP_QUICKACK, &on, sizeof(on));
        setsockopt(listen_fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on));

        if (bind(listen_fd, (const struct sockaddr *)listen_addr, sizeof(struct sockaddr_in)) < 0)
                return -1;

        if (listen(listen_fd, BACKLOG_SIZE))
                return -1;

        return listen_fd;
}

/**
 * @brief Read data to buffer from given client.
 *
 * @param[in] c Source client.
 * @param[in] pkt Pointer to destination buffer_t
 * @return 1 on success, -1 on case of error, 0 in case we should try to read data again later.
 */
static int read_data(client *c, buffer_t *pkt)
{
        int rc = -1;
        volatile int rcv;

        if (pkt == NULL || pkt->buffer == NULL || c->fd <= 0)
                return rc;

        while (pkt->offset < (int)pkt->buf_len) {
                rcv = recv(c->fd, pkt->buffer + pkt->offset, pkt->buf_len - pkt->offset, 0);

                if (rcv < 0) {
                        if (errno == EAGAIN || errno == EWOULDBLOCK)
                                rc = 0;
                        else
                        rc = -1;

                        break;
                }

                if (rcv == 0) {
                        rc = -1;
                        break;
                }

                pkt->rcv += rcv;
                pkt->offset += rcv;
                rc = 1;
        }

        return rc;
}

/**
 * @brief Send data from buffer to given client.
 *
 * @param[in] c Destination client.
 * @param[in] pkt Pointer to source buffer_t
 * @return 1 on success, -1 on case of error, 0 in case we still have data to send.
 */
static int send_data(client *c, buffer_t *pkt)
{
        int rc = -1;
        volatile int snt = 0;

        if (pkt == NULL || pkt->buffer == NULL || c->fd <= 0) return rc;

        while (pkt->rcv) {
                snt = send(c->fd, pkt->buffer + (pkt->offset - pkt->rcv), pkt->rcv, 0);

                if (snt <= 0) {
                        if (errno == EAGAIN || errno == EWOULDBLOCK)
                                rc = 0;
                        else
                                rc = -1;
                        break;
                }

                pkt->rcv -= snt;
        }

        if (pkt->rcv == 0) {
                pkt->offset = 0;
                rc = 1;
        }

        return rc;
}

/**
 * @brief Callback executed when data from socket maybe read.
 *
 * @param[in] loop Pointer to ev_loop(main loop structrure)
 * @param[in] w Pointer to ev_io(watcher for current FD)
 * @param[in] revents - Eventmask.
 */
static void on_read(struct ev_loop *loop, ev_io *w, int revents)
{
        int rc, fd = w->fd;
        buffer_t *buf = (buffer_t *)w->data;

        client *c = get_client(fd);

        if (revents & EV_ERROR) {
                fprintf(stderr, "[%d] Internal error.\n", fd);
                del_client(c->fd);
                return;
        }

        if (revents & EV_READ && buf->offset < (int)buf->buf_len) {
                rc = read_data(c, buf);

                if (rc >= 0) {
                        fprintf(stderr, "[%d] RECEIVED: %.*s\n", c->fd, buf->rcv, buf->buffer);
                        rc = send_data(c, buf);

                        if (rc == 0)
                                ev_io_set(w, c->fd, EV_WRITE | EV_READ);
                        
                        c->ts = ev_time();                                
                }

                if (rc < 0) {
                        del_client(c->fd);
                        return;
                }
        }

        if (revents & EV_WRITE && buf->rcv > 0) {
                rc = send_data(c, buf);

                if (rc == 1)
                        ev_io_set(w, c->fd, EV_READ);

                if (rc < 0) {
                        del_client(c->fd);
                        return;
                }
        }
}

/**
 * @brief Callback executed when new connection maybe accepted.
 *
 * @param[in] loop Pointer to ev_loop(main loop structrure)
 * @param[in] w Pointer to ev_io(watcher for current FD)
 * @param[in] revents - Eventmask.
 */
static void accept_connect(struct ev_loop *loop, ev_io *w, __attribute__((unused)) int revents)
{
        struct sockaddr_in sa;
        socklen_t sa_len = sizeof(sa);
        buffer_t *buf = NULL;

        client *c = (client *)calloc(1, sizeof(client));

        if ((c->io = (struct ev_io *)calloc(1, sizeof(struct ev_io))) == NULL)
                err(EXIT_FAILURE, "calloc() ");

        if ((c->fd = accept(w->fd, (struct sockaddr *)&sa, &sa_len)) <= 0)
                return;

        fcntl(c->fd, F_SETFL, O_NONBLOCK);

        printf("[%d] Accept new connection from %s:%d\n", c->fd, inet_ntoa(sa.sin_addr),
                ntohs(sa.sin_port));

        if ((buf = calloc(1, sizeof(buffer_t))) == NULL)
                err(EXIT_FAILURE, "calloc()");
        if ((buf->buffer = calloc(BUFFER_SIZE, sizeof(char))) == NULL)
                err(EXIT_FAILURE, "calloc()");
        buf->buf_len = BUFFER_SIZE;

        ev_io_init(c->io, on_read, c->fd, EV_READ);
        c->io->data = buf;
        ev_io_start(loop, c->io);

        c->ts = ev_time();
        c->loop = loop;
        int len = strlen(inet_ntoa(sa.sin_addr)) + 1;
        c->host = malloc(len);
        strncpy(c->host, inet_ntoa(sa.sin_addr), len);

        add_client(c);
        free(c->host);
        free(c);
}

/**
 * @brief Timer callback for detect idle clients.
 *
 * @param[in] w Pointer to ev_timer.
 * @param[in] revents - Eventmask.
 */
static void timeout_cb(EV_P_ ev_timer *w, int revents)
{
        ev_tstamp now = ev_time();
        check_clients_timeout(now);
}

/**
 * @brief Init and start tcp server.
 *
 * @param[in] port Listened port.
 */
int server_start(int port)
{
        struct sockaddr_in sock = {0};
        struct ev_loop *loop = NULL;
        ev_io sock_watcher;
        ev_timer timer_watcher;
        int s_fd;

        if (port < 1024 || port > 65535) {
                fprintf(stderr, "Uncorrect port %d. Available range is [1024-65535].\n", port);
                return -1;
        }

        sock.sin_port = htons(port);
        sock.sin_addr.s_addr = htonl(INADDR_ANY);
        sock.sin_family = AF_INET;

        if ((s_fd = listen_on_socket(&sock)) == -1) {
                fprintf(stderr, "listen_on_socket()\n");
                return -1;
        } else
                fprintf(stderr, "[%d] ECHO server listen on port %d\n", s_fd, port);

        if ((loop = ev_loop_new(EVFLAG_NOENV | EVBACKEND_EPOLL)) == NULL) {
                fprintf(stderr, "Couldn't create main eventloop!");
                close(s_fd);
                return -1;
        }

        ev_io_init(&sock_watcher, accept_connect, s_fd, EV_READ);
        ev_io_start(loop, &sock_watcher);

        ev_timer_init(&timer_watcher, timeout_cb, 0.1, 0.1);
        ev_timer_start(loop, &timer_watcher);

        ev_run(loop, 0);

        return 0;
}