#ifndef __INTERFACE_H
#define __INTERFACE_H

int init_netlink_socket();
void close_netlink_socket();
int configure_interface(const char *ifname, const char *address);
int deconfigure_interface(const char *ifname, const char *address);

#endif