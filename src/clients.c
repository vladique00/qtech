#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "clients.h"

static client *clients_tbl;

static void cleanup_client (client *c);

/**
 * @brief Dump table of clients.
 */
void dump_clients()
{
        client *tmp = clients_tbl;
        if (tmp == NULL)
                fprintf(stdout, "Clients not exists!\n");
        else {
                fprintf(stdout, "| fd |     host      |        ts         |\n");
                for (int i = 0; tmp != NULL; tmp = tmp->next, i++)
                        printf("| #%d | %s | %f |\n", tmp->fd, tmp->host, tmp->ts);
        }
}

/**
 * @brief Adds an item to the clients table.
 *
 * @param[in] c Pointer to the client. Should be freed after use.
 */
void add_client (client *c)
{
        client *last = clients_tbl;
        client *tmp = (client *) calloc(1, sizeof(client));
        
        tmp->fd = c->fd;
        tmp->ts = c->ts;
        tmp->io = c->io;
        tmp->loop = c->loop;
        tmp->next = NULL;

        int len = strlen(c->host) + 1;
        tmp->host = malloc(len);
        strncpy(tmp->host, c->host, len);

        if (clients_tbl == NULL) {
                clients_tbl = tmp;
                return;
        }

        client *prev = last;
        for (last = clients_tbl; last != NULL; last = last->next) {
                if (last->fd > tmp->fd) {
                        if (prev == last) {
                                tmp->next = last;
                                clients_tbl = tmp;
                        } else {
                                prev->next = tmp;
                                tmp->next = last;
                        }
                        return;
                }
                prev = last;
        }

        prev->next = tmp;
}


/**
 * @brief Delete clients from table by fd.
 *
 * @param[in] fd FD of client.
 */
void del_client (int fd)
{
        client *last = clients_tbl;
        client *prev = last;

        if (NULL == last)
                return;

        if (last->fd == fd) {
                fprintf(stdout, "[%d] Disconnect client %s\n", last->fd, last->host);
                clients_tbl = last->next;
                cleanup_client(last);
                return;
        }

        for (; last != NULL; last = last->next) {
                if (last->fd == fd) {
                        fprintf(stdout, "[%d] Disconnect client %s\n", last->fd, last->host);
                        if (NULL != last->next) {
                                prev->next = last->next;
                                cleanup_client(last);
                                last = prev;
                        }
                        else {
                                prev->next = NULL;
                                cleanup_client(last);
                        }
                return;
                }
                prev = last;
        }
}

/**
 * @brief Get client by fd.
 *
 * @param[in] fd FD of the target client.
 */
client *get_client (int fd)
{
        client *last = clients_tbl;

        if (NULL == last)
                return NULL;

        for (; last != NULL; last = last->next)
                if (last->fd == fd)
                return last;

        return NULL;
}

/**
 * @brief Iterate through clients table for cleanup timed out clients.
 *
 * @param[in] now Current timestamp.
 */
void check_clients_timeout (ev_tstamp now)
{
        client *tmp = clients_tbl;
        for (; tmp != NULL; tmp = tmp->next) {
                if (tmp->ts + 5.0 < now) {
                        printf("Host [%d] -> %s is timed out!\n", tmp->fd, tmp->host);
                        del_client(tmp->fd);
                }
        }
}

/**
 * @brief Cleanup client.
 *
 * @param[in] c Pointer to the client.
 */
static void cleanup_client (client *c)
{
        buffer_t *buf = (buffer_t *)(c->io->data);
        ev_io_stop(c->loop, c->io);
        close(c->fd);
        free(c->io);
        free(buf->buffer);
        free(buf);
        free(c->host);
        c->next = NULL;
        free(c);
}