#ifndef __CLIENTS_H
#define __CLIENTS_H

#include <ev.h>

typedef struct client_s {
        int fd;
        char *host;
        ev_tstamp ts;
        struct ev_io *io;
        struct ev_loop *loop;
        struct client_s *next;
} client;

typedef struct __pkt_t_ {
    char *buffer;
    volatile int rcv;
    volatile int offset;
    size_t buf_len;
} buffer_t;

void add_client (client *c);
client *get_client (int fd);
void del_client (int fd);
void dump_clients();
void check_clients_timeout(ev_tstamp now);

#endif