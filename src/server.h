#ifndef __SERVER_H_
#define __SERVER_H_

#include <ev.h>

#define BACKLOG_SIZE 64
#define BUFFER_SIZE 256

int server_start(int port);

#endif