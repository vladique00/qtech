#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>

#include <sys/types.h>
#include <arpa/inet.h>

#include <net/if.h>

#define NETLINK_BUFF_SIZE 4096
#define NETLINK_RQ_PID ((__u32) getpid())

static int nl_sock = -1;
static char *nl_buff = NULL;

static int set_link_state(int if_idx, int state);
static int configure_address(int if_idx, const char *addr, ushort msg_type);
static int configure_gateway(int if_idx, const char *addr);

static void nlhdr_fill(struct nlmsghdr **nl_header, ushort size, ushort msg_type, ushort flags);
static int get_msg();

static int pton(int domain, void *buf, const char *ip);

/**
 * @brief Netlink socket/buffer initialization.
 *
 * @return 0 on success, -1 on case of error.
 */
int init_netlink_socket()
{
        nl_buff = malloc(NETLINK_BUFF_SIZE);
        if (nl_buff == NULL) {
                printf("Failed malloc nl_buff!\n");
                goto error;
        }

        nl_sock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
        if (nl_sock < 0) {
                printf("Failed to open socket while dumping interfaces!\n");
                goto error;
        }

        return 0;

error:
        if (nl_buff != NULL)
                free(nl_buff);
        return -1;
}

/**
 * @brief Close Netlink socket.
 */
void close_netlink_socket()
{
        if (nl_buff != NULL)
                free(nl_buff);
        if (nl_sock != -1)
                close(nl_sock);
}

/**
 * @brief Enable. Configure link state, address, gateway of interface.
 *
 * @param[in] ifname Source client.
 * @param[in] address Target IP address.
 * @return 0 on success, -1 on case of error.
 */
int configure_interface(const char *ifname, const char *address)
{
        int if_idx = if_nametoindex(ifname);
        
        if (set_link_state(if_idx, 0) < 0) {
                fprintf(stderr, "Set link to state DOWN is failed!\n");
                return -1;
        }

        if (set_link_state(if_idx, 1) < 0) {
                fprintf(stderr, "Set link to state UP is failed!\n");
                return -1;
        }

        if (configure_address(if_idx, address, RTM_NEWADDR) < 0) {
                fprintf(stderr, "Set address is failed!\n");
                return -1;
        }

        // if (configure_gateway(if_idx, "192.168.0.1") < 0) {
        //         fprintf(stderr, "Set gateway is failed!\n");
        //         return -1;
        // }
        return 0;
}

/**
 * @brief Disable. Link state to DOWN. Delete address.
 *
 * @param[in] ifname Source client.
 * @param[in] address Target IP address.
 * @return 0 on success, -1 on case of error.
 */
int deconfigure_interface(const char *ifname, const char *address)
{
        int if_idx = if_nametoindex(ifname);

        if (set_link_state(if_idx, 0) < 0) {
                fprintf(stderr, "Set link to state DOWN is failed!\n");
                return -1;
        }

        if (configure_address(if_idx, address, RTM_DELADDR) < 0) {
                fprintf(stderr, "Delete address is failed!\n");
                return -1;
        }
        return 1;
}

/**
 * @brief Fill Netlink msg header.
 *
 * @param[in, out] nl_header Source client.
 * @param[in] size Size of msg.
 * @param[in] msg_type Netlink msg type.
 * @param[in] flags Flags.
 */
static void nlhdr_fill(struct nlmsghdr **nl_header, ushort size, ushort msg_type, ushort flags)
{
        struct nlmsghdr *nlh = *nl_header;
        memset(nl_buff, 0, NLMSG_LENGTH(size));
        nlh->nlmsg_len = NLMSG_LENGTH(size);
        nlh->nlmsg_type = msg_type;
        nlh->nlmsg_flags = flags;
        nlh->nlmsg_pid = NETLINK_RQ_PID;
}

/**
 * @brief Set network interface link state.
 *
 * @param[in] if_idx Interface ID.
 * @param[in] state 0 on IFDOWN, 1 on case IFUP.
 * @return Response err code on msg.
 */
static int set_link_state(int if_idx, int state)
{
        struct nlmsghdr *nl_header = (struct nlmsghdr *) nl_buff;
        struct ifinfomsg *ifi = (struct ifinfomsg *)NLMSG_DATA(nl_header);
        nlhdr_fill(&nl_header, sizeof(*ifi), RTM_SETLINK, NLM_F_REQUEST | NLM_F_ACK);

        ifi->ifi_index = if_idx;
        ifi->ifi_change |= IFF_UP;
        ifi->ifi_family = AF_UNSPEC;
        ifi->ifi_flags = state;

        send(nl_sock, nl_buff, nl_header->nlmsg_len, 0);
        return get_msg();
}


/**
 * @brief Set IP address for interface.
 *
 * @param[in] if_idx Interface ID.
 * @param[in] addr Target IP address.
 * @param[in] msg_type RTM_NEWADDR to set address or RTM_DELADDR for delete address.
 * @return Response err code on msg.
 */
static int configure_address(int if_idx, const char *addr, ushort msg_type)
{
        struct nlmsghdr *nl_header = (struct nlmsghdr *) nl_buff;
        struct ifaddrmsg *ifa = (struct ifaddrmsg *)NLMSG_DATA(nl_header);
        nlhdr_fill(&nl_header, sizeof(*ifa), msg_type, NLM_F_REQUEST | NLM_F_REPLACE | NLM_F_ACK);

        ifa->ifa_family = AF_INET;
        ifa->ifa_index = if_idx;
        ifa->ifa_flags = IFA_F_PERMANENT;
        ifa->ifa_scope = 0;
        ifa->ifa_prefixlen = 24;

        int ip_len = 0;

	struct rtattr *rta = (struct rtattr*)IFA_RTA(ifa);
	rta->rta_type = IFA_LOCAL;
	ip_len = pton(ifa->ifa_family, RTA_DATA(rta), addr);
	rta->rta_len = RTA_LENGTH(ip_len);
	nl_header->nlmsg_len = NLMSG_ALIGN(nl_header->nlmsg_len) + rta->rta_len;

	int l = NETLINK_BUFF_SIZE - nl_header->nlmsg_len;
	rta = (struct rtattr*)RTA_NEXT(rta, l);
	rta->rta_type = IFA_ADDRESS;
	ip_len = pton(ifa->ifa_family, RTA_DATA(rta), addr);
	rta->rta_len = RTA_LENGTH(ip_len);
	nl_header->nlmsg_len += rta->rta_len;

        send(nl_sock, nl_buff, nl_header->nlmsg_len, 0);
        return get_msg();
}

/**
 * @brief Set Gateway address for interface.
 *
 * @param[in] if_idx Interface ID.
 * @param[in] addr Target IP address.
 * @return Response err code on msg.
 */
static int configure_gateway(int if_idx, const char *addr)
{
        struct nlmsghdr *nl_header = (struct nlmsghdr *) nl_buff;
        struct rtmsg *rt = (struct rtmsg*)NLMSG_DATA(nl_header);
        nlhdr_fill(&nl_header, sizeof(*rt), RTM_NEWROUTE, NLM_F_REQUEST | NLM_F_REPLACE | NLM_F_CREATE | NLM_F_ACK);

	rt->rtm_family = AF_INET;
	rt->rtm_table = RT_TABLE_MAIN;
	rt->rtm_protocol = RTPROT_STATIC;
	rt->rtm_scope = RT_SCOPE_UNIVERSE;
	rt->rtm_type = RTN_UNICAST;

        int ip_len = 0;

	struct rtattr *rta = (struct rtattr*)RTM_RTA(rt);
	rta->rta_type = RTA_GATEWAY;
	ip_len = pton(rt->rtm_family, RTA_DATA(rta), addr);
	rta->rta_len = RTA_LENGTH(ip_len);
	nl_header->nlmsg_len = NLMSG_ALIGN(nl_header->nlmsg_len) + rta->rta_len;

	int l = NETLINK_BUFF_SIZE - nl_header->nlmsg_len;
	rta = (struct rtattr*)RTA_NEXT(rta, l);
	rta->rta_type = RTA_OIF;
	rta->rta_len = RTA_LENGTH(sizeof(int));
	*((int*)RTA_DATA(rta)) = if_idx;
	nl_header->nlmsg_len += rta->rta_len;

        send(nl_sock, nl_buff, nl_header->nlmsg_len, 0);
        return get_msg();
}

/**
 * @brief Receive response on netlink msg.
 *
 * @return Response err code on msg.
 */
static int get_msg()
{
        struct nlmsghdr *nl_header;

        int len = recv(nl_sock, nl_buff, NETLINK_BUFF_SIZE, 0);
        nl_header = (struct nlmsghdr *) nl_buff;

        for (; NLMSG_OK(nl_header, len); nl_header = NLMSG_NEXT(nl_header, len)) {
                if (nl_header->nlmsg_type == NLMSG_ERROR) {
                        struct nlmsgerr *err = (struct nlmsgerr *)NLMSG_DATA(nl_header);
                        if (err->error != 0) {
                                printf("%s\n", strerror(-err->error));
                                return err->error;
                        } else 
                                return 0;
                }
        }
        return -1;
}

static int pton(int domain, void *buff, const char *ip)
{
	if (domain == AF_INET) {
		if (!inet_pton(AF_INET, ip, buff)) {
                        fprintf(stderr, "Invalid address %s!\n", ip);
                        return -1;
                }
		return sizeof(struct in_addr);
	}

	if (domain == AF_INET6) {
		if (!inet_pton(AF_INET6, ip, buff)) {
                        fprintf(stderr, "Invalid address %s!\n", ip);
                        return -1;
                }
		return sizeof(struct in6_addr);
	}
        fprintf(stderr, "Unsupported domain!\n");
	return -1;
}
