#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>

#include <net/if.h>

#include <signal.h>

#include "server.h"
#include "interface.h"

static void signalHandler(int sig);

static char ifname[10] = {0};
static char address[16] = {0};
// char gateway[16] = {0};

int main (int argc, char *argv[]) 
{
        int port = -1, flags = 0;

        int opt;
        while ((opt = getopt(argc, argv, "p:i:a:g:")) != -1) {
                switch (opt) {
                case 'i':
                        strcpy(ifname, optarg);
                        flags |= (1 << 0);
                        break;
                case 'a':
                        strcpy(address, optarg);
                        flags |= (1 << 1);
                        break;
                // case 'g':
                //         strcpy(gateway, optarg);
                //         break;
                case 'p':
                        port = atoi(optarg);
                        flags |= (1 << 2);
                        break;
                default:
                        printf("Usage: %s [-p port] [-i ifname]\n", argv[0]);
                        exit(EXIT_FAILURE);
                }
        }

        if (flags != 0x07) {
                printf("Usage: %s [-i ifname] [-a address] [-p port]\n", argv[0]);
                exit(EXIT_FAILURE);
        }

        /* Init signal */
        struct sigaction sa;
        sa.sa_handler = signalHandler;
        sa.sa_flags = 0;
        sigemptyset(&sa.sa_mask);
        sigaction(SIGTERM, &sa, NULL);
        sigaction(SIGINT, &sa, NULL);

        /* Check interface */
        if (!if_nametoindex(ifname)) {
                fprintf(stderr, "Interface %s not found!\n", ifname);
                exit(EXIT_FAILURE);
        }

        /* Init netlink socket and manage interface */
        if (init_netlink_socket() < 0) {
                fprintf(stderr, "Netlink socket init failed!\n");
                exit(EXIT_FAILURE);
        }

        if (configure_interface(ifname, address) < 0) {
                fprintf(stderr, "Configuring interface is failed!\n");
                exit(EXIT_FAILURE); 
        }

        /* Server */
        if (server_start(port) < 0) {
                fprintf(stderr, "Start server failed!\n");
                exit(EXIT_FAILURE);
        }

}

static void signalHandler(int sig) {
        switch (sig) {
        case SIGINT:
        case SIGKILL:
        case SIGTERM:
                fprintf(stdout, "Got a interrupt signal. Exiting.\n");
                if (deconfigure_interface(ifname, address) < 0) {
                        fprintf(stdout, "Interface deconfiguration completed incorrectly.");
                }
                close_netlink_socket();
                exit(EXIT_SUCCESS);
                break;
        }
}
